#! /usr/bin/env nix-shell
#! nix-shell -i python -p "python3.withPackages (ps: with ps; [ aiohttp orjson ])"
"""
Axione CityFast Eligibility Data Downloader
Copyright (C) 2022  Thibault Lemaire <thibault.lemaire@protonmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Ce script télécharge les données de l'API publique d'Axione.
Voir index.html pour les afficher sur une carte.

Si vous travaillez pour Axione, veuillez consulter https://gitlab.com/ThibaultLemaire/axione-elig-map
"""

import asyncio
from datetime import datetime
import logging
import random
import typing as t

import aiohttp
import orjson

# EDIT ME
# Rectangle à requêter
FETCH_AREA = (
    45.1051, # Sud
    45.2253, # Nord
    5.6406,  # Ouest
    5.7809,  # Est
)
# Remplacez DEBUG par INFO pour désactiver les messages console.
LOGGING_LEVEL = logging.DEBUG

# Nombre de sous-divisions du rectangle initial.
# Plus ce nombre est élevé, plus les zones requêtées seront petites.
# Des zones plus petites répondent plus vite, mais signifient plus de requêtes.
STARTING_SLICES = 2048

MAX_PARALLEL_REQUESTS = 16

# D'après résultats empiriques: Si au bout d'1min le serveur n'a rien répondu, il renvoie '[]'
# Mettre plus de 60s fausse donc les résultats
REQUEST_TIMEOUT_SECONDS = 30
SUBSLICES_ON_TIMEOUT = 3
JSON_FILE_PATH = "./database.json"
AXIONE_API = 'https://ws-e-ftth.axione.fr/wsinterne/gui/public/punaiseselig.json?listepto'
SERIALIZATION_OPTIONS = orjson.OPT_NAIVE_UTC | orjson.OPT_INDENT_2 | orjson.OPT_UTC_Z


class BoundingBox(t.NamedTuple):
    sud: float
    nord: float
    ouest: float
    est: float

class Address(t.TypedDict):
    # Champs venant de l'API
    id: str
    x: float
    y: float
    statutElig: t.Union[
            t.Literal['COMMERCIALISABLE'],
            t.Literal['NON_ELIGIBLE']]
    ... # champs omis

    # Ajouté pour suivre des mise à jour incrémentales
    last_updated: datetime

def load_data_file() -> t.Dict[str, Address]:
    with open(JSON_FILE_PATH, 'rb') as data_file:
        raw_data = data_file.read()
    return {address['id']: address
            for address
            in orjson.loads(raw_data)}

def save_data_file(addresses: t.Dict[str, Address]):
    raw_data = orjson.dumps(list(addresses.values()), option=SERIALIZATION_OPTIONS)
    with open(JSON_FILE_PATH, 'wb') as data_file:
        data_file.write(raw_data)

def subchunks(area: BoundingBox, slices: int) -> t.Iterator[BoundingBox]:
    if slices == 1:
        yield area
        return

    subslices = slices // 2
    extra = slices % 2
    sud, nord, ouest, est = area
    x = est - ouest
    y = nord - sud
    if x > y:
        middle = ouest + x / 2
        yield from subchunks(
                BoundingBox(
                    ouest=ouest,
                    est=middle,
                    sud=sud,
                    nord=nord),
                subslices)
        yield from subchunks(
                BoundingBox(
                    ouest=middle,
                    est=est,
                    sud=sud,
                    nord=nord),
                subslices + extra)
    else:
        middle = sud + y / 2
        yield from subchunks(
                BoundingBox(
                    ouest=ouest,
                    est=est,
                    sud=sud,
                    nord=middle),
                subslices)
        yield from subchunks(
                BoundingBox(
                    ouest=ouest,
                    est=est,
                    sud=middle,
                    nord=nord),
                subslices + extra)


async def main():
    logging.debug("Begin DoS attack...") #humour
    database = load_data_file()
    new_data = asyncio.Event()

    async def database_syncer():
        while True:
            try:
                await new_data.wait()
            except asyncio.CancelledError:
                return

            save_data_file(database)
            new_data.clear()
            logging.debug("Data synced.")


    syncer = asyncio.create_task(database_syncer())

    tiles = list(subchunks(FETCH_AREA, STARTING_SLICES))
    random.shuffle(tiles)
    jobs = asyncio.Queue()
    for chunk in tiles:
        jobs.put_nowait(chunk)

    async with aiohttp.ClientSession(
            timeout=aiohttp.ClientTimeout(total=REQUEST_TIMEOUT_SECONDS),
            raise_for_status=False,
            ) as session:

        async def worker():
            while True:
                try:
                    area = await jobs.get()
                except asyncio.CancelledError:
                    return

                try:
                    async with session.get(
                            AXIONE_API,
                            params=area._asdict()) as resp:
                        data = orjson.loads(await resp.text())
                except asyncio.TimeoutError:
                    logging.debug(f"Timed out. Subdividing. {area}")
                    for chunk in subchunks(area, SUBSLICES_ON_TIMEOUT):
                        jobs.put_nowait(chunk)
                    jobs.task_done()
                    continue

                for address in data:
                    address['last_updated'] = datetime.now()
                    database[address['id']] = address
                if data:
                    new_data.set()
                jobs.task_done()
                logging.debug(f"Loaded {len(data)} addresses for {area}")


        workers = [asyncio.create_task(worker()) for _ in range(MAX_PARALLEL_REQUESTS)]
        done, _pending = await asyncio.wait(
                [asyncio.create_task(jobs.join()), syncer] + workers,
                return_when=asyncio.FIRST_COMPLETED)
        for task in workers:
            task.cancel()
        await asyncio.gather(*workers)

    syncer.cancel()
    await syncer


if __name__ == "__main__":
    logging.basicConfig(level=LOGGING_LEVEL)
    asyncio.run(main())
